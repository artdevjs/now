const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const torrent2magnet = require('torrent2magnet');

require('dotenv').config();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(helmet());

app.get('*', (req, res) => {
  res.set('Content-Type', 'application/json');
  res.status(200).json({ route: req.originalUrl });
});

app.post('*', (req, res) => {
  torrent2magnet(req.body.url).then(uri => {
    return res.json({ uri: uri });
  }).catch(err => {
    throw new Error(err);
  });
});

// app.listen(process.env.PORT, () =>
//   console.log(`Local app listening on port ${process.env.PORT}!`),
// );


module.exports = app;